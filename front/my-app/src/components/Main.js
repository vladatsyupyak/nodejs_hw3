import React from "react";
import {Link, Route, Routes} from "react-router-dom"
import "../App.css"

function Main(){
    return (
       <div className="main_page wrapper">
           <p className="main_title">Choose your role</p>
          <div className="roles_wrapp">
              <div className="role">
                  <Link to="/driver" >
                      driver
                  </Link>
              </div>
              <div className="role role_shipper">
                  <Link to="/shipper" className=" ">
                      shipper
                  </Link>
              </div>
          </div>
       </div>
    )
}
export default Main