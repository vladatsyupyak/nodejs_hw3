import React from "react";
import {Link, Route, Routes} from "react-router-dom"

function Navbar() {
    return (
            <nav className="navbar navbar-expand-lg  navbar-light bg-light">
                <div className="container-fluid flex-lg-column ">
                    <Link to={"/shipper/page"} className="navbar-brand" href="#">UTRUCK</Link>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                            data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav flex-lg-column  mb-2 mb-lg-0">
                            <li className="nav-item">
                                <Link to={`/shipper/page`} className="nav-link">
                                    main
                                </Link>
                            </li>
                            <Link to={`/shipper/loads/newLoads`} className="nav-link">
                                New Loads
                            </Link>
                            <Link to={`/shipper/loads/postedLoads`} className="nav-link">
                                Posted Loads
                            </Link>


                            <Link to={`/shipper/loads/history`} className="nav-link">
                                History
                            </Link>
                            <Link to={`/shipper/loads/profile`} className="nav-link">
                                Profile
                            </Link>
                        </ul>
                    </div>
                </div>
                </nav>
    )
}

export default Navbar