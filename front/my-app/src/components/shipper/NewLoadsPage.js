import {Link} from "react-router-dom";
import React, {useEffect, useState} from "react"
import Navbar from "./Navbar";

function PostResultInfo(obj) {
    console.log(obj.driverFound)
    let text = 'Sorry, driver was not found. Please try again later'
    if (obj.driverFound) {
        text = 'Driver was found'
    }
    return <div className="modal fade" id="exampleModal" tabIndex="-1" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
        <div className="modal-dialog">
            <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLabel">The result of searching a driver</h5>
                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div className="modal-body">
                    {text}
                </div>
                <div className="modal-footer">
                    <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
}

function NewLoadsPage() {
    const [loads, setLoads] = useState([])
    const [postResult, setPostResult] = useState({})

    function onPostClick(load) {
        let id = load._id
        console.log(id)
        fetch(`http://localhost:8080/api/loads/${id}/post`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + sessionStorage.getItem('jwt_token')
            },
        })
            .then((response) => response.json())
            .then((data) => {
                console.log('Success:', data);
                if (!data.driver_found) {
                    setPostResult({...postResult, showInfo: true, found: false, id: id})
                } else {
                    setPostResult({...postResult, showInfo: true, found: true, id: id})

                }
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    }

    function getNewLoads() {
        return fetch('http://localhost:8080/api/loads/new', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
                'Authorization': 'Bearer ' + sessionStorage.getItem('jwt_token')
            },
        })
            .then((response) => response.json())
            .then((res) => {
                setLoads(res.loads)
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    }

    useEffect(() => {
        getNewLoads()
    }, [])

    return (
        <div>
            <Navbar/>
            {loads.length > 0 && (
                <div className="wrapper loads_page_wrapp">
                    <div className="load_title_wrap">
                        <p className="new_loads_title">LOAD NAME</p>
                        <p className="new_loads_title">DELIVERY ADDRESS</p>
                        <p className="new_loads_title">PICKUP ADDRESS</p>
                        <p className="new_loads_title">CREATED AT:</p>


                    </div>
                    {loads.map(load => {
                            if (postResult.found && load._id === postResult.id) {
                                return (<div key={load._id} className="loads_wrapp">
                                        <p>{load.name}</p>
                                        <p>{load.delivery_address}</p>
                                        <p>{load.pickup_address}</p>
                                        <p>{load.createdAt}</p>
                                        <button disabled={true} onClick={() => onPostClick(load)}>Post</button>
                                    </div>
                                )
                            } else {
                                return (<div className="loads_wrapp" key={load._id}>
                                        <p>{load.name}</p>
                                        <p>{load.delivery_address}</p>
                                        <p>{load.pickup_address}</p>
                                        <p>{load.createdAt}</p>
                                        <PostResultInfo driverFound={postResult.found}/>
                                        <button type="button" className="post_btn btn btn-primary" data-bs-toggle="modal"
                                                data-bs-target="#exampleModal" disabled={false}
                                                onClick={() => onPostClick(load)}>Post
                                        </button>
                                    </div>
                                )
                            }
                        }
                    )}

                </div>
            )}
        </div>
    )

}

export default NewLoadsPage;
