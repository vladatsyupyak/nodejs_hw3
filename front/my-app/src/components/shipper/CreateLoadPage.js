import {Link, useNavigate} from "react-router-dom";
import React, {useEffect, useState} from "react"
import Navbar from "./Navbar";

function LoadInfoPopup(props) {
    let load = props.load
    let navigate = useNavigate();
    const routeChange = () => {
        let path = `/shipper/page`;
        navigate(path);
    }
    return (
        <div className="overlay">
            <div className="modal_load_info">
                <h3>Load Info:</h3>
                <div className="modal-body">
                    <p>Name of load: <span className="check_load_info_span">{load.name}</span></p>
                    <p>Payload: <span className="check_load_info_span">{load.payload}</span></p>
                    <p>Delivery address: <span className="check_load_info_span">{load.delivery_address}</span></p>
                    <p>Pickup address: <span className="check_load_info_span">{load.pickup_address}</span></p>
                    <p>Width: <span className="check_load_info_span">{load.dimensions.width}</span></p>
                    <p>Height: <span className="check_load_info_span">{load.dimensions.height}</span></p>
                    <p>Length: <span className="check_load_info_span">{load.dimensions.length}</span></p>

                    <input className="btn btn-primary" onClick={() => routeChange()} type="submit" name="confirm"/>
                </div>
            </div>
        </div>
)
}

function CreateLoadPage() {
    const [load, setLoad] = useState({})
    const [showPopup, setPopup] = useState(false)

    const submit = e => {
        e.preventDefault()
        console.log(load)
        fetch('http://localhost:8080/api/loads/', {
            method: 'POST',
            body: JSON.stringify(load),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + sessionStorage.getItem('jwt_token')
            },
        })
            .then((response) => response.json())
            .then((data) => {
                console.log('Success:', data);
            })
            .then(() => setPopup(true))
            // .then(()=>routeChange())
            .catch((error) => {
                console.error('Error:', error);
            });
    }

    return (
        <div>
            <Navbar/>
            <div className="wrapper create_load_wrapper">

                <p className="create_load_title">create load</p>
                <form onSubmit={submit} className="form">
                    <input
                        className="form_input"
                        placeholder="name"
                        type="text"
                        onChange={e => setLoad({...load, name: e.target.value})}
                    />

                    <input
                        className="form_input"

                        placeholder="payload"
                        type="text"
                        onChange={e => setLoad({...load, payload: e.target.value})}
                    />
                    <input
                        className="form_input"

                        placeholder="pickup"
                        type="text"
                        onChange={e => setLoad({...load, pickup_address: e.target.value})}
                    />
                    <input
                        className="form_input"

                        placeholder="deliver"
                        type="text"
                        onChange={e => setLoad({...load, delivery_address: e.target.value})}
                    />
                    <input
                        className="form_input"

                        placeholder="width"
                        type="text"
                        onChange={e => {
                            // setDimensions({ ...dimensions, width: e.target.value})
                            setLoad((prev) => ({...prev, dimensions: {...prev.dimensions, width: e.target.value}}))

                        }}
                    />
                    <input
                        className="form_input"
                        placeholder="height"
                        type="text"
                        onChange={e => {
                            setLoad({...load, dimensions: {...load.dimensions, height: e.target.value}})

                        }}
                    />
                    <input
                        className="form_input"
                        placeholder="length"
                        type="text"
                        onChange={e => {
                            setLoad({...load, dimensions: {...load.dimensions, length: e.target.value}})

                        }}
                    />
                    <input type="submit" className="btn btn-success" name="Sign Up"/>
                </form>
                {showPopup ? <LoadInfoPopup load={load}/> : null}
            </div>
        </div>

    )
}

export default CreateLoadPage;
