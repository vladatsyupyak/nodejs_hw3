import {Link, useNavigate} from "react-router-dom";
import React, {useEffect, useState} from "react"
import Navbar from "./Navbar";


function LoadInfoPopup(props) {
    let load = props.load
    let navigate = useNavigate();
    const routeChange = () => {
        let path = `/shipper/page`;
        navigate(path);
    }
    return (
        <div>
            <p>{load.name}</p>
            <p>{load.payload}</p>
            <p>{load.delivery_address}</p>
            <p>{load.pickup_address}</p>
            <p>{load.dimensions.width}</p>
            <input onClick={() => routeChange()} type="submit" name="confirm"/>
        </div>
    )
}

function PostedLoadsPage() {
    const [loads, setLoads] = useState([])
    const [postResult, setPostResult] = useState({})


    function getNewLoads() {
        return fetch('http://localhost:8080/api/loads?offset=0&limit=0', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
                'Authorization': 'Bearer ' + sessionStorage.getItem('jwt_token')
            },
        })
            .then((response) => response.json())
            .then((res) => {
                console.log(res.loads)
                setLoads(res.loads)
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    }

    useEffect(() => {
        getNewLoads()
    }, [])

    return (
        <div>
            <Navbar/>
            {loads.length > 0 && (
                <div className="wrapper  loads_page_wrapp">
                    <div className="load_title_wrap load_posted_title_wrap">
                        <p className="new_loads_title">LOAD NAME</p>
                        <p className="new_loads_title">DELIVERY ADDRESS</p>
                        <p className="new_loads_title">PICKUP ADDRESS</p>
                        <p className="new_loads_title">STATE</p>
                        <p className="new_loads_title">CREATED AT:</p>
                    </div>
                    {loads.map(load => {
                            if (load.status === 'ASSIGNED') {
                                return (<div className="posted_loads_wrapp loads_wrapp" key={load._id}>
                                        <p>{load.name}</p>
                                        <p>{load.delivery_address}</p>
                                        <p>{load.pickup_address}</p>
                                        <p>{load.state}</p>
                                        <p>{load.createdAt}</p>
                                    </div>
                                )
                            }


                        }
                    )}

                </div>
            )}
        </div>
    )

}

export default PostedLoadsPage;
