import {Link, Route, Routes} from "react-router-dom";
import React, {useEffect, useState} from "react"
import Navbar from "./Navbar";
import NewLoadsPage from "./NewLoadsPage";
import PostedLoadsPage from "./PostedLoadsPage";
import CreateLoadPage from "./CreateLoadPage";
import ShipperProfile from "./ShipperProfile";

function ShipperPage() {
    const [user, setUsers] = useState({})

    function getUserByFetch() {
        return fetch('http://localhost:8080/api/users/me', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
                'Authorization': 'Bearer ' + sessionStorage.getItem('jwt_token')
            },
        })
            .then((response) => response.json())
            .then((res) => {
                console.log(res.user)
                setUsers(res.user)
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    }

    useEffect(() => {
        getUserByFetch()
    }, [])


    return (

    <div>
        <Navbar/>
       <div className="wrapper shipper_page">
           <header>
               <p>Welcome {user.email} </p>
           </header>
           <Link to={`/shipper/loads/createLoad`} className="nav-link create_load_link">
               Create Loads
           </Link>
       </div>
       </div>
    )
}

export default ShipperPage;
