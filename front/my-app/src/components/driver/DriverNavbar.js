import React from "react";
import {Link, Route, Routes} from "react-router-dom"

function DriverNavbar() {
    return (
        <nav className="navbar navbar-expand-lg  navbar-light bg-light">
            <div className="container-fluid flex-lg-column ">
                <Link to={"/driver/page"} className="navbar-brand" href="#">UTRUCK</Link>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav flex-lg-column  mb-2 mb-lg-0">
                        <li className="nav-item">
                            <Link to={`/driver/page`} className="nav-link">
                                main
                            </Link>
                        </li>
                        <Link to={`/driver/allTrucks`} className="nav-link">
                            All trucks
                        </Link>
                        <Link to={`/driver/assignedLoad`} className="nav-link">
                            Assigned Load
                        </Link>
                        <Link to={`/driver/profile`} className="nav-link">
                            Profile
                        </Link>
                    </ul>
                </div>
            </div>
        </nav>
    )
}

export default DriverNavbar