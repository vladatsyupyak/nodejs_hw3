import {Link, useNavigate} from "react-router-dom";
import React, {useEffect, useState} from "react"
import Navbar from "./DriverNavbar";


function DeleteTruck(props) {

    let text = 'The truck has been deleted'
    return (<div className="overlay">
            <div className="modal_load_info">
                <div className="modal-body">
                    <p>{text}</p>
                    <button className="btn btn-dark" onClick={props.closeFunction}>close</button>
                </div>
            </div>
        </div>
    )
}

function AllTrucks() {


    const [trucks, setTrucks] = useState([])
    const [deleteModal, setDeleteModal] = useState(false)


    function onDeleteClick(e, id) {
        e.target.parentElement.classList.add('hidden')
        return fetch(`http://localhost:8080/api/trucks/${id}`, {
            method: 'DELETE',
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
                'Authorization': 'Bearer ' + sessionStorage.getItem('jwt_token')
            },
        })
            .then((response) => response.json())
            .then((res) => {
                console.log(res)

            })
            .then(() => {
                    setDeleteModal(true)
                }
            )
            .catch((error) => {
                console.error('Error:', error);
            });

    }

    function onOkClick() {
        setDeleteModal(false)
    }

    function onAssignClick(e, truck) {
        e.target.classList.add('assigned')
        return fetch(`http://localhost:8080/api/trucks/${truck._id}/assign`, {
            method: 'POST',
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
                'Authorization': 'Bearer ' + sessionStorage.getItem('jwt_token')
            },
        })
            .then((response) => response.json())
            .then((res) => {
                console.log(res)

            })
            .catch((error) => {
                console.error('Error:', error);
            });

    }


    function getNewLoads() {
        return fetch('http://localhost:8080/api/trucks', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
                'Authorization': 'Bearer ' + sessionStorage.getItem('jwt_token')
            },
        })
            .then((response) => response.json())
            .then((res) => {
                console.log(res.trucks)
                setTrucks(res.trucks)
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    }

    let d = false
    useEffect(() => {
        getNewLoads()
    }, [])
    return (
        <div>
            <Navbar/>
            {trucks.length > 0 && (
                <div className="wrapper loads_page_wrapp">
                    <div className="load_title_wrap load_posted_title_wrap">
                        <p className="new_loads_title"> TRUCK TYPE</p>
                        <p className="new_loads_title">ADDED AT:</p>
                    </div>


                    {
                        trucks.map(truck => {
                                let btnStyle = "post_btn btn btn-primary"
                                if (truck.status) {
                                    btnStyle = "post_btn btn btn-primary assigned"
                                    d = true
                                }
                                return (<div className="loads_wrapp trucks_wrapp posted_loads_wrapp" key={truck._id}>
                                        <p>{truck.type}</p>
                                        <p>{truck.createdAt}</p>
                                        <button type="button" className={btnStyle} disabled={d}
                                                onClick={(e) => onAssignClick(e, truck)}>ASSIGN
                                        </button>

                                        <button type="button" onClick={(e) => onDeleteClick(e,truck._id)} className="btn delete_btn btn-dark"
                                                data-toggle="modal" data-target="#exampleModalCenter">
                                            delete
                                        </button>
                                    </div>
                                )


                            }
                        )}
                    {deleteModal ? <DeleteTruck closeFunction={onOkClick}/> : null}

                </div>
            )}
        </div>
    )

}

export default AllTrucks;
