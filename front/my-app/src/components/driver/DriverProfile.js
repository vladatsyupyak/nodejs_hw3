import React from 'react';

import {Link, useNavigate} from "react-router-dom";
import {useEffect, useState} from "react";
import Navbar from "./DriverNavbar";

function ShipperProfile() {
    let navigate = useNavigate();
    const routeChange = () => {
        let path = `/`;
        navigate(path);
    }
    const [user, setUsers] = useState({})

    function getUserByFetch() {
        return fetch('http://localhost:8080/api/users/me', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
                'Authorization': 'Bearer ' + sessionStorage.getItem('jwt_token')
            },
        })
            .then((response) => response.json())
            .then((res) => {
                console.log(res.user)
                setUsers(res.user)
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    }
    useEffect(() => {
        getUserByFetch()
    }, [])


    return (
        <div>
            <Navbar/>
            <div className="wrapper">
                <div className="profile_wrapper">
                    <h3>Your profile</h3>
                    {/*<p>email: {user.email}</p>*/}
                    <p className="profile_info"><span className="profile_info_span">email:</span> {user.email}</p>
                    <button className="btn change_pass_btn btn-primary">change password</button>

                </div>
                <button onClick={routeChange} className="logout_btn btn btn-primary">logout</button>
            </div>


        </div>
    )

}

export default ShipperProfile;

