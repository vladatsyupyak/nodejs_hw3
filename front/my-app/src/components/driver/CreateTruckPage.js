import React, {useState} from "react";
import {useNavigate} from "react-router-dom";



function CreateTruck() {
    const [showCreateBtn, setCreateBtn] = useState(true)
    const [showPopup, setPopup] = useState(false)
    const Dropdown = () => {

        const [value, setValue] = React.useState('SPRINTER');

        const sendTruck = truck => {
            console.log('works')
            console.log(truck)
            fetch('http://localhost:8080/api/trucks/', {
                method: 'POST',
                body: JSON.stringify({type: truck}),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + sessionStorage.getItem('jwt_token')
                },
            })
                .then((response) => response.json())
                .then((data)=>{
                    setPopup(false)
                setCreateBtn(true)
                return data})
                .then((data) => {
                    console.log('Success:', data);
                })
                .catch((error) => {
                    console.error('Error:', error);
                });
        }



        const handleChange = (event) => {
            console.log(event.target.value)
            setValue(event.target.value);
        };

        return (
            <div className="dropdown_wrap">
                <label>
                    choose the type of truck
                    <select value={value} onChange={handleChange}>
                        <option value="SPRINTER">SPRINTER</option>
                        <option value="LARGE_STRAIGHT">LARGE_STRAIGHT</option>
                        <option value="SMALL_STRAIGHT">SMALL_STRAIGHT</option>
                    </select>
                </label>
                <button className="btn btn-primary" onClick={()=>{sendTruck(value)}}>submit</button>
            </div>
        );
    };



    const onclick = () => {
        setPopup(true)
        setCreateBtn(false)
    }

    return (
        <div>
            <div className="wrapper create_load_wrapper">
                {showPopup ? <Dropdown /> : null}
                {showCreateBtn ?   <button className='create_load_link create_truck_btn' onClick={onclick}>create truck</button>
                : null}
            </div>
        </div>

    )
}
export default CreateTruck