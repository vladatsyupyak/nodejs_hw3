import {Link, Route, Routes, useNavigate} from "react-router-dom";
import React, {useEffect, useState} from "react"

import DriverNavbar from "./DriverNavbar";
import Navbar from "../shipper/Navbar";
import CreateTruckPage from "./CreateTruckPage";




function DriverPage() {
    const [user, setUsers] = useState({})

    function getUserByFetch() {
        return fetch('http://localhost:8080/api/users/me', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
                'Authorization': 'Bearer ' + sessionStorage.getItem('jwt_token')
            },
        })
            .then((response) => response.json())
            .then((res) => {
                console.log(res.user)
                setUsers(res.user)
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    }

    useEffect(() => {
        getUserByFetch()
    }, [])


    return (

        <div>
            <DriverNavbar/>
            <div className="wrapper shipper_page">
                <header>
                    <p>Welcome {user.email} </p>
                </header>
                <CreateTruckPage/>
            </div>
        </div>
    )
}

export default DriverPage;
