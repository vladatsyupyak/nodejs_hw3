import {Link} from "react-router-dom";
import React, {useEffect, useState} from "react"
import Navbar from "./DriverNavbar";


function AssignedLoadPage() {
    const [loads, setLoads] = useState({})


    function getAssignedLoad() {
        return fetch('http://localhost:8080/api/loads/active', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
                'Authorization': 'Bearer ' + sessionStorage.getItem('jwt_token')
            },
        })
            .then((response) => response.json())
            .then((res) => {
                setLoads(res.load)
                console.log(loads)
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    }

    useEffect(() => {
        getAssignedLoad()
    }, [])

    return (
        <div>
            <Navbar/>

            <div className="wrapper loads_page_wrapp">
                <div className="load_title_wrap">
                    <p className="new_loads_title">LOAD NAME</p>
                    <p className="new_loads_title">DELIVERY ADDRESS</p>
                    <p className="new_loads_title">PICKUP ADDRESS</p>
                    <p className="new_loads_title">CREATED AT:</p>


                </div>

                {loads ? <div className="loads_wrapp" key={loads._id}>
                    <p>{loads.name}</p>
                    <p>{loads.delivery_address}</p>
                    <p>{loads.pickup_address}</p>
                    <p>{loads.createdAt}</p>
                </div> : null}


            </div>

        </div>
    )

}

export default AssignedLoadPage;
