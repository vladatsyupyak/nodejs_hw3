import React from "react";
import {Link, Route, Routes, useNavigate} from "react-router-dom"

function Authorization(props) {
    const [user, setUser] = React.useState('')
    let navigate = useNavigate();
    const routeChange = () => {
        let path = `/${props.role}/page`;
        navigate(path);
    }

    const submit = e => {
        console.log(user)
        e.preventDefault()
        fetch('http://localhost:8080/api/auth/login', {
            method: 'POST',
            body: JSON.stringify(user),
            headers: {'Content-Type': 'application/json'},
        })
            .then((response) => response.json())
            .then((data) => {
                console.log('Success:', data.jwt_token);
                return data.jwt_token
            })
            .then((data) => sessionStorage.setItem('jwt_token', data))
            .then(() => routeChange())
            .catch((error) => {
                console.error('Error:', error);
            });
    }

    return (
        <div className="login_wrapp">
            <form className="form" id="form-signin" onSubmit={submit}>
                <h1 className="h3 mb-3 fw-normal">Please sign in</h1>
                <div className="form-floating">
                    <input  id="loginUsername"
                            className="form-control"
                            placeholder="email"
                        type="text"
                        onChange={e => setUser({...user, email: e.target.value})}
                    />
                    <label htmlFor="loginUsername">Username</label>
                </div>
                <div className="form-floating">
                    <input id="loginPassword"
                           className="form-control"
                        placeholder="password"
                        type="text"
                        onChange={e => setUser({...user, password: e.target.value})}
                    />
                    <label htmlFor="loginPassword">Password</label>
                </div>
                <button className="w-100 btn btn-lg btn-primary" id="loginSubmit" type="submit">Sign in</button>
                <p className="link_to_signup">
                    Don`t have an account?
                    <Link  to={`/${props.role}/registration`} className="site-title">
                        Sign Up
                    </Link>
                </p>
            </form>

        </div>
    )
}

export default Authorization;
