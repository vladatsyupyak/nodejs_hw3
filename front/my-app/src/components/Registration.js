import React from 'react';
import Authorization from "./Authorization";
import {Link, useNavigate} from "react-router-dom";

function Registration(props) {
    const [user, setUser] = React.useState({role: props.role})
    let navigate = useNavigate();
    const routeChange = () =>{
        let path = `/${props.role}/`;
        navigate(path);
    }
    const submit = e => {
        console.log(user)
        e.preventDefault()
        fetch('http://localhost:8080/api/auth/register', {
            method: 'POST',
            body: JSON.stringify(user),
            headers: { 'Content-Type': 'application/json' },
        })
            .then((response) => response.json())
            .then((data) => {
                console.log('Success:', data);
            })
            .then(()=>routeChange())
            .catch((error) => {
                console.error('Error:', error);
            });
        // return <Authorization/>
    }

    return (
        <div>
            <div className="login_wrapp">
                <form className="form" id="form-signin" onSubmit={submit}>
                    <h1 className="h3 mb-3 fw-normal">Register, please</h1>
                    <div className="form-floating">
                        <input  id="loginUsername"
                                className="form-control"
                                placeholder="email"
                                type="text"
                                onChange={e => setUser({...user, email: e.target.value})}
                        />
                        <label htmlFor="loginUsername">Username</label>
                    </div>
                    <div className="form-floating">
                        <input id="loginPassword"
                               className="form-control"
                               placeholder="password"
                               type="text"
                               onChange={e => setUser({...user, password: e.target.value})}
                        />
                        <label htmlFor="loginPassword">Password</label>
                    </div>
                    <button className="w-100 btn btn-lg btn-primary" id="loginSubmit" type="submit">Sign up</button>
                </form>

            </div>


        </div>





    )
}

export default Registration;
