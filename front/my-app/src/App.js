import Authorization from "./components/Authorization";
import {Link, Route, Routes} from "react-router-dom"
import Registration from "./components/Registration";
import DriverPage from "./components/driver/DriverPage";
import ShipperPage from "./components/shipper/ShipperPage";
import Main from "./components/Main";
import NewLoadsPage from "./components/shipper/NewLoadsPage";
import ShipperProfile from "./components/shipper/ShipperProfile";
import CreateLoadPage from "./components/shipper/CreateLoadPage";
import PostedLoadsPage from "./components/shipper/PostedLoadsPage";
import Navbar from "./components/shipper/Navbar";
import HistoryPage from "./components/shipper/History";
import CreateTruckPage from "./components/driver/CreateTruckPage";
import AllTrucksPage from "./components/driver/AllTrucksPage";
import AssignedTruck from "./components/driver/AssignedLoad";
import AssignedLoadPage from "./components/driver/AssignedLoad";
import DriverProfile from "./components/driver/DriverProfile";


function App() {
    return (
        <div className="App">
            <Routes>
                <Route path="/" element={<Main/>}/>
                <Route path="/driver" element={<Authorization role={'driver'}/>}/>
                <Route path="/driver/page" element={<DriverPage/>}/>
                <Route path="/shipper/page" element={<ShipperPage/>}/>
                <Route path="/shipper/loads/newLoads" element={<NewLoadsPage/>}/>
                <Route path="/shipper/loads/createLoad" element={<CreateLoadPage/>}/>
                <Route path="/shipper/loads/postedLoads" element={<PostedLoadsPage/>}/>
                <Route path="/shipper/loads/history" element={<HistoryPage/>}/>
                <Route path="/shipper/loads/profile" element={<ShipperProfile/>}/>
                <Route path="/shipper" element={<Authorization role={'shipper'}/>}/>
                <Route path="/driver/registration" element={<Registration role={'DRIVER'}/>}/>
                <Route path="/shipper/registration" element={<Registration role={'SHIPPER'}/>}/>

                <Route path="/driver/createTruck" element={<CreateTruckPage/>}/>
                <Route path="/driver/assignedLoad" element={<AssignedLoadPage/>}/>
                <Route path="/driver/profile" element={<DriverProfile/>}/>

                <Route path="/driver/allTrucks" element={<AllTrucksPage/>}/>
            </Routes>
        </div>
    );
}

export default App;
