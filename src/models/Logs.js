const mongoose = require('mongoose');

const logSchema = mongoose.Schema(
    {
        reqBody: {
            type: String,

        },
        function: {
            type: String,

        },

    },
    { timestamps: true },
);
const Log = mongoose.model('Log', logSchema);

module.exports = {
    Log,
};
