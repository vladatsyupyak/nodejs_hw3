const mongoose = require('mongoose');

const truckSchema = mongoose.Schema(
    {
        created_by: {
            type: String,
            required: true,
        },
        assigned_to: {
            type: String,

        },
        status: {
            type: String,
        },
        type: {
            type: String,
            required: true,

        }
    },
    { timestamps: true },
);
const Truck = mongoose.model('Truck', truckSchema);

module.exports = {
    Truck,
};
