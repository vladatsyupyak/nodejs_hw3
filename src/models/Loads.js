const mongoose = require('mongoose');

const loadSchema = mongoose.Schema(
    {
        created_by: {
            type: String,
            required: true,
        },
        assigned_to: {
            type: String,

        },
        status: {
            type: String,
            required: true,
        },
        state: {
            type: String
        },
        dimensions: {
            type: {
                width: {
                    type: Number
                },
                length: {
                    type: Number
                },
                height: {
                    type: Number
                }
            }


        },
        payload: {
            type: Number,
            required: true
        },
        name: {
            type: String
        },
        pickup_address: {
            type: String,
            required: true
        },
        delivery_address: {
            type: String,
            required: true
        },
        logs: {
            type : {
                message: {
                    type: String
                },
                time: {
                    type: String
                }
            }

        }
    },
    {timestamps: true},
);
const Load = mongoose.model('Load', loadSchema);

module.exports = {
    Load,
};
