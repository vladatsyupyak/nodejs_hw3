const {Load} = require('../models/Loads');
const {Truck} = require('../models/Trucks');

const jwt = require("jsonwebtoken");
const {now} = require("mongoose");
require('dotenv').config();

function chooseTruckType(dimensions, payload) {
    let {width, length, height} = dimensions
    if (width <= 350 && width > 250 || length <= 700 && length > 500 || height <= 200 && height > 170 || payload <= 4000 && payload > 2500) {
        return ["LARGE_STRAIGHT"]
    } else if (width > 170 || length > 300 || height <= 170 || payload > 1700) {
        return ["LARGE_STRAIGHT", "SMALL_STRAIGHT"]
    } else {
        return ["LARGE_STRAIGHT", "SMALL_STRAIGHT", "SPRINTER"]
    }
}

function getTokenPayload(req) {
    const {authorization} = req.headers;
    const [, token] = authorization.split(' ');
    const tokenPayload = jwt.verify(token, process.env.SECRET_KEY);
    return tokenPayload;
}

function createLoad(req, res, next) {
    const {name, payload, pickup_address, delivery_address, dimensions} = req.body;
    console.log(dimensions)
    const user = getTokenPayload(req);
    const load = new Load({
        created_by: user.userId,
        name,
        payload,
        pickup_address,
        delivery_address,
        dimensions,
        assigned_to: null,
        status: "NEW"
    });
    console.log(load)
    Load.create(load)
        .then(() => res.json({"message": "Truck created successfully"}))
        .catch((err) => {
            console.log(err)
            next(err);
        });
}

async function getLoadsInfo(req, res) {
    const user = getTokenPayload(req);
    const {offset} = req.query;
    const {limit} = req.query;
    const loads = await Load.find({created_by: user.userId}).skip(offset).limit(limit);
    res.send({
        count: await Load.count(), loads,
    });
}
async function getNewLoadsInfo(req, res) {
    const user = getTokenPayload(req);
    const loads = await Load.find({created_by: user.userId, status: "NEW"});
    console.log(loads)
    res.send({
        loads
    });
}
const getLoadById = async (req, res) => {
    const {id} = req.params;
    const load = await Load.findById(id);
    res.send({load});
};

const updateLoadById = async (req, res) => {
    const {id} = req.params;
    const fieldToUpdate = req.body
    const load = await Load.findByIdAndUpdate(id, {"$set": fieldToUpdate});
    res.status(200).send({
        message: '"Truck details changed successfully"', load
    });
};

const postLoad = async (req, res) => {
    const {id} = req.params;
    const load = await Load.findById(id)
    const truckType = chooseTruckType(load.dimensions, load.payload)
    console.log(truckType)
    const truck = await Truck.findOne({"status": "IS", "type": {$in: truckType}});
    console.log(truck)
    if (truck) {
        await Load.findByIdAndUpdate(id, {
            status: "ASSIGNED", assigned_to: truck.assigned_to, state: "En route to Pick Up", logs: {
                message: `Load assigned to driver with id ${truck.assigned_to}`, time: new Date(now())
            }
        })
        await Truck.findByIdAndUpdate(truck._id, {
            "$set": {
                status: "OL",
            }
        })
        res.status(200).send({
            message: '"Truck details changed successfully"', driver_found: true
        });
    } else {
        await Load.findByIdAndUpdate(id, {
            status: "NEW",
        })
        res.status(200).send({
            message: '"Truck details changed successfully"', driver_found: false
        });
    }

}

const deleteLoad = async (req, res) => {
    const {id} = req.params;
    const load = await Load.findByIdAndDelete(id);

    res.status(200).send({
        message: '"Truck details changed successfully"', load
    });
};

async function getUserActiveLoad(req, res) {
    const user = getTokenPayload(req);
    console.log(user.userId)
    const load = await Load.findOne({assigned_to: user.userId, state: {$ne: "SHIPPED"}});
    console.log(load)
    res.send({
        load
    });
}

async function iterateLoadToNextState(req, res) {
    const user = getTokenPayload(req);
    const activeLoad = await Load.findOne({assigned_to: user.userId, state: {$ne: "SHIPPED"}});
   if(activeLoad){
       let state = activeLoad.state
       let nextState = ''
       if (state === "En route to Pick Up") {
           nextState = 'Arrived to Pick Up'
       } else if (state === "Arrived to Pick Up") {
           nextState = 'En route to delivery'
       } else {
           nextState = 'Arrived to delivery'
       }
       console.log(nextState)
       if (nextState === 'Arrived to delivery') {
           await Load.findByIdAndUpdate(activeLoad._id, {state: nextState, status: 'SHIPPED'},);
           await Truck.findOneAndUpdate({assigned_to: activeLoad.assigned_to}, {status: 'IS'})
       }
       await Load.findByIdAndUpdate(activeLoad._id, {state: nextState});

       res.send({
           "message": "Load state changed to " + nextState
       });
   } else{  res.send({
       "message": "no active loads"
   });

   }
}

const getShippingInfo = async (req, res) => {
    const {id} = req.params;
    const load = await Load.findById(id);
    const truckId = load.assigned_to
    const truck = await Truck.findById(truckId);

    res.send({
        load, truck
    });
};
module.exports = {
    createLoad,
    getLoadsInfo,
    getLoadById,
    updateLoadById,
    deleteLoad,
    postLoad,
    getUserActiveLoad,
    iterateLoadToNextState,
    getShippingInfo,
    getNewLoadsInfo
};