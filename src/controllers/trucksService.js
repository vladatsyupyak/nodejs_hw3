const {Truck} = require('../models/Trucks');
const {Log} = require('../models/Logs');

const jwt = require("jsonwebtoken");
require('dotenv').config();


function getTokenPayload(req) {
    const {authorization} = req.headers;
    const [, token] = authorization.split(' ');
    const tokenPayload = jwt.verify(token, process.env.SECRET_KEY);
    return tokenPayload;
}

async function createTruck(req, res, next) {
    console.log("here" + req.body)

    const {type} = req.body;
    const user = getTokenPayload(req);
    const truck = new Truck({
        created_by: user.userId,
        type,
        assigned_to: null,
        status: null,
    });
    const log = new Log({reqBody: String(req.body), function: "createTruck"}
    )
   await log.save()
    Truck.create(truck)
        .then(() => res.json({"message": "Truck created successfully"}))
        .catch((err) => res.json({"message": err})
        );

}

async function getTrucksInfo(req, res) {
    const user = getTokenPayload(req);
    const trucks = await Truck.find({created_by: user.userId})
    const log = new Log({reqBody: trucks[0]._id, function: "getTrucksInfo"}
    )
    await log.save()
    if (!trucks) {
        res.status(500).send({
            message: "trucks were not found"
        });
    }
    res.send({
        count: await Truck.count(),
        trucks,
    });
}

const getTruckById = async (req, res) => {
    const {id} = req.params;
    const log = new Log({reqBody: String(req.body), function: "getTruckById"}
    )
    await log.save()
    const truck = await Truck.findById(id);

    res.send({truck});
    return truck
};

const deleteTruck = async (req, res) => {
    const {id} = req.params;
    const truck = await Truck.findByIdAndDelete(id);

    res.send({"message": "Truck deleted successfully"});
};

const updateTruckById = async (req, res) => {
    const {id} = req.params;
    const fieldToUpdate = Object.keys(req.body)[0]
    const truck = await Truck.findByIdAndUpdate(id, {[fieldToUpdate]: req.body[fieldToUpdate]});
    res.status(200).send({
        message: '"Truck details changed successfully"',
        truck
    });
};
const assignTruck = async (req, res) => {
    const {id} = req.params;
    const user = getTokenPayload(req);
    let truck = await Truck.findByIdAndUpdate(id, {assigned_to: user.userId, status: "IS"});
    const log = new Log({reqBody: String(req.body), function: "getTruckById"}
    )
    await log.save()
    console.log('here1' + truck)
    if (truck) {
       return  res.status(200).send({
            message: '"Truck assigned successfully"',
            assigned_to: user.userId,
            truck
        });
    } else {
       return res.status(400).send({
            message: '"Error"',

        });
    }
};
module.exports = {
    getTrucksInfo,
    createTruck,
    getTruckById,
    updateTruckById,
    assignTruck,
    deleteTruck
};