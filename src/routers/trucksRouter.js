const express = require('express');

const router = express.Router();
const {authMiddleware} = require('../middleware/authMiddleware');
const {getTrucksInfo, createTruck, getTruckById, updateTruckById, assignTruck, deleteTruck} = require("../controllers/trucksService");


router.get('/',authMiddleware, getTrucksInfo);
router.post('/', authMiddleware, createTruck);
router.get('/:id', authMiddleware,  getTruckById);
router.delete('/:id', authMiddleware,  deleteTruck);

router.post('/:id/assign', authMiddleware,  assignTruck);

router.post('/:id', authMiddleware,  updateTruckById);






module.exports = {
    trucksRouter: router,
};
