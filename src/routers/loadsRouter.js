const express = require('express');

const router = express.Router();
const {authMiddleware} = require('../middleware/authMiddleware');
const {createLoad, getLoadsInfo, getLoadById, updateLoadById, deleteLoad, postLoad, getUserActiveLoad,
    iterateLoadToNextState, getShippingInfo, getNewLoadsInfo
} = require("../controllers/loadsService");


router.post('/', authMiddleware,createLoad );
router.get('/',authMiddleware, getLoadsInfo);
router.get('/active/', authMiddleware,getUserActiveLoad  );
router.get('/new/', authMiddleware,getNewLoadsInfo);

router.get('/:id/shipping_info', authMiddleware,getShippingInfo  );

router.patch('/active/state', authMiddleware,iterateLoadToNextState  );


router.get('/:id', authMiddleware,getLoadById  );

router.put('/:id', authMiddleware, updateLoadById  );
router.delete('/:id', authMiddleware, deleteLoad  );
router.post('/:id/post', authMiddleware, postLoad  );



// router.post('/:id/assign', authMiddleware,  );






module.exports = {
    loadsRouter: router,
};
